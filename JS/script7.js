
// Завдання 7
// Доповніть код так, щоб він коректно працював

const array = ['value', () => 'showValue'];

// Допишіть код тут

const [value, showValue] = array;

console.log(value); // має бути виведено 'value'
console.log(showValue());  // має бути виведено 'showValue'